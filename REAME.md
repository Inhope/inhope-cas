# 部署
1. /configs/deployerConfigContext.xml 复制到 /WEB-INF/deployerConfigContext.xml


# 以下是参考-

law.in-hope.cn | 信公小安
sso.in-hope.cn | sso 二级域名

【微信AppKey】
公众号ID：gh_cc50b41aec47
AppID(应用ID): wx793dd5ea1bff1aca
AppSecret(应用密钥): edc8a8e59f934ccd2b1c84ca148f3e76


【新浪微博AppKey】
App Key：3336540564
App Secret：84433dc0d37f3a56a64b6aeb2b3875d4 


## 需要修改的配置
deployerConfigContext.xml

1. 第三方登录

<property name="callbackUrl" value="http://app.leanote.com/cas/login" />

<bean id="weiXin" class="com.inhope.weixin.WeiXinClient">
    <property name="key" value="wx9ee29ae4bfc1db3d" />
    <property name="secret" value="440ed1cd69e7cde0c73cabaec8caa746" />
    <property name="userService" ref="userService"/>
</bean>

<bean id="weibo" class="com.inhope.weibo.WeiboClient">
    <property name="key" value="3136348150" />
    <property name="secret" value="d446301c1a581d160d070009847cec01" />
    <property name="userService" ref="userService"/>
</bean>

2. 数据库配置

<bean id="dataSource"
      class="org.springframework.jdbc.datasource.DriverManagerDataSource">
    <property name="driverClassName" value="com.mysql.jdbc.Driver" />
    <property name="url"
              value="jdbc:mysql://inhope.leanote.top:3306/inhope?characterEncoding=utf-8&amp;autoReconnect=true" />
    <property name="username" value="test" />
    <property name="password" value="abc123" />
</bean>
