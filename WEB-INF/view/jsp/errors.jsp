<%@ page pageEncoding="UTF-8" %>

<jsp:directive.include file="default/ui/includes/boxTop.jsp" />

<div id="msg" class="alert alert-danger center">
  <h2>错误</h2>
  <p>
    无效或过时的登录请求
    <!--
    <spring:message code="screen.unavailable.message" />
    -->
  </p>

  <div class="footer-a">
      <a href="login">登录</a>
  </div>
</div>

<jsp:directive.include file="default/ui/includes/boxBottom.jsp" />

