<!DOCTYPE html>

<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html lang="en">
<head>
  <meta charset="UTF-8" />
  <title>Inhope用户认证中心</title>
  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" >
  <link href="libs/bootstrap.min.css" rel="stylesheet">
  <link href="libs/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/index.css?i=2" rel="stylesheet">

  <style>
    .alert{display: block;}
    #msg {
      /*width: 400px;*/
      margin: auto;
    }
  </style>
</head>
