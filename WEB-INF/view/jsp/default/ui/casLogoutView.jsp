<%@ page pageEncoding="UTF-8" %>

<jsp:directive.include file="includes/boxTop.jsp" />

    <div id="msg" class="alert alert-success center">

      <h2><spring:message code="screen.logout.header" /></h2>

      <p><spring:message code="screen.logout.success" /></p>
      <p><spring:message code="screen.logout.security" /></p>

    <div class="footer-a">
        <a href="login">登录</a>
    </div>
    </div>

<jsp:directive.include file="includes/boxBottom.jsp" />
