
<jsp:directive.include file="includes/top.jsp" />
<body class="alert alert-danger">
  <div id="msg" class="errors">
    <h2><spring:message code="screen.badworkstation.heading" /></h2>
    <p><spring:message code="screen.badworkstation.message" /></p>
  </div>
<jsp:directive.include file="includes/bottom.jsp" />
