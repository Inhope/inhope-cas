<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.util.*"%>
<jsp:directive.include file="includes/top.jsp" />

<%
  Date d = new Date();
%>

<body id="boxBody" class="style-<%=(d.getTime() % 5)%>">
<!-- <div id="particles-js"></div> -->

<table id="box" class="" style="width: 100%;height: 100%;padding:0">
    <tr><td style="vertical-align: middle">
  <div style="height: 545px;">
    <h1 id="logo">
      <a href="/" style="display: block; position: relative"><img src="images/xiaoi-logo.png" style="width: 60px;
    margin-left: -144px;">
          <span style="position: absolute;
    display: inline-block;
    font-size: 36px;
    color: #cb780c;
    color: #ff9f4c;
    margin-top: 31px;margin-left: 10px;">信公小安
          </span></a>
    </h1>

    <div id="boxForm">

      <form:form method="post" id="fm1" commandName="${commandName}" htmlEscape="true">

        <!-- 错误信息 -->
        <%--<form:errors path="*" id="msg" cssClass="alert alert-danger" element="div" htmlEscape="false" />--%>

        <c:forEach items="${flowRequestContext.messageContext.allMessages}" var="message">
          <div class="alert
            <c:choose>
              <c:when test="${message.severity == 'ERROR'}">
                alert-danger
              </c:when>
              <c:when test="${message.severity == 'INFO'}">
                alert-warning
              </c:when>
              <c:otherwise>
                alert-info
              </c:otherwise>
            </c:choose>
            ">${message.text}</div>
        </c:forEach>

        <!--
        <div style="background: rgba(255, 255, 255, 0.48);
    margin-bottom: 10px;
    padding: 3px;
    border-radius: 3px;">
          测试账户:
          <ul style="list-style:none;padding-left:15px;">
            <li>用户名: admin 密码: abc123</li>
            <li>用户名: test 密码: abc123</li></ul>
        </div>
        -->

        <div class="form-group-ctn">

          <div class="form-group form-group-username">
            <c:choose>
              <c:when test="${not empty sessionScope.openIdLocalId}">
                <strong>${sessionScope.openIdLocalId}</strong>
                <input class="form-username form-control" placeholder="用户名/邮箱/手机" type="hidden" id="username" name="username" value="${sessionScope.openIdLocalId}" />
              </c:when>
              <c:otherwise>
                <spring:message code="screen.welcome.label.netid.accesskey" var="userNameAccessKey" />
                <form:input cssClass="form-username form-control" placeholder="用户名/邮箱/手机" cssErrorClass="error" id="username" tabindex="1" accesskey="${userNameAccessKey}" path="username" autocomplete="off" htmlEscape="true" />
              </c:otherwise>
            </c:choose>
          </div>
          <div class="form-group form-group-pwd">
            <spring:message code="screen.welcome.label.password.accesskey" var="passwordAccessKey" />
            <form:password cssClass="form-pwd form-control" placeholder="密码" cssErrorClass="error" id="password" tabindex="2" path="password"  accesskey="${passwordAccessKey}" htmlEscape="true" autocomplete="off" />
          </div>

          <div class="form-group form-group-captcha">
            <div style="overflow: hidden;">
              <input id="captcha" name="captcha" placeholder="验证码" class="form-captcha form-control" tabindex="3" type="text" value="" size="22" autocomplete="off">
              <img class="captcha-image" style="margin-top:-10px;" id="captcha-image" alt="必须输入验证码" title="点击刷新验证码" src="/captchaImg" onclick="this.src='/captchaImg?'+Math.random();">
              <a style="color: #ef9c66;vertical-align: middle;" onclick="$('#captcha-image').get(0).src='/captchaImg?'+Math.random();" class="no-select">换一换</a>
            </div>
          </div>
        </div>

        <input type="hidden" name="lt" value="${loginTicket}" />
        <input type="hidden" name="execution" value="${flowExecutionKey}" />
        <input type="hidden" name="_eventId" value="submit" />

        <div class="clearfix auto-forget no-select" >
          <div class="pull-left checkbox"><label><input type="checkbox" name="rememberMe"> 自动登录</label></div>
          <a href="http://xiaoan.in-hope.cn/findPassword" class="forget pull-right">忘记密码 ?</a>
        </div>

        <input class="btn btn-default btn-submit" name="submit" accesskey="l" value="<spring:message code="screen.welcome.button.login" />" tabindex="4" type="submit" />

        <a href="http://xiaoan.in-hope.cn/register" class="btn btn-default btn-register">注册</a>

        <div class="third">
          <a href="${WeiXinClientUrl}" class="weichat"><i class="fa fa-wechat"></i></a>
          <a href="${WeiboClientUrl}" class="weibo"><i class="fa fa-weibo"></i></a>
        </div>

      </form:form>

    </div>
  </div>
</td></tr>
</table>

<script src="libs/jquery-1.9.0.min.js"></script>
<script src="libs/bootstrap.min.js"></script>
<script>
  /*
  #boxBody {
  background: url(../images/bg/3.pic.jpg) no-repeat center top #1a1a1a;
  background-size: 100% auto;
  }
  */
  /*
  var i = 0;
  setInterval(function () {
    i = i % 5;
    $('#boxBody').css({'background': 'url(/images/bg/' + (i + 1) + '.pic.jpg) no-repeat center top #1a1a1a', 'background-size': '100% auto'});
    i++;
  }, 4000);
  */
</script>

</body>
</html>
